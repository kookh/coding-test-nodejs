const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");

app.use(bodyParser.json());
app.use(cors());
app.options("*", cors());
app.use(morgan("dev"));

require("./app/router/router.js")(app);

module.exports = app;