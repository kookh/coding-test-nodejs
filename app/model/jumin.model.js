module.exports = (sequelize, Sequelize) => {
	const Jumin = sequelize.define('jumins', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		jumin_number: {
			type: Sequelize.STRING,
			allowNull: false
		},
		is_right_jumin: {
			type: Sequelize.BOOLEAN,
			allowNull: false
		}
	}, {
		timestamps: true,
		paranoid: true,
		underscored: true
	});

	return Jumin;
}