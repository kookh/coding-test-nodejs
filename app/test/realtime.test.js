/**
 * @jest-environment node
 */
const request = require("supertest");
const app = require("../../server.js");

/**
 * API 테스트
 */
test("GET /api/v1/realtime", async (done) => {
  const response = await request(app).get("/api/v1/realtime");
  expect(response.status).toBe(200);
  expect(response.body.message).toBe("네이버 실시간 검색어를 반환합니다.");
  expect.arrayContaining(response.body.data);
  done();
});
