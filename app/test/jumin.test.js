const juminService = require("../service/jumin.service.js");
const request = require("supertest");
const app = require("../../server.js");
const db = require("../config/db.config.js");
const Jumin = db.jumin;
/**
 * 유닛 테스트
 */
test("주민 번호가 올바른 경우", () => {
  expect(juminService.verifyJumin("881124-1234564")).toStrictEqual(true);
});

test("주민 번호가 잘못된 경우", () => {
  expect(juminService.verifyJumin("881124-1234565")).toStrictEqual(false);
  expect(juminService.verifyJumin("881124-1234566")).toStrictEqual(false);
});

/**
 * API 테스트 (추가, 리스트, 삭제)
 */
test("주민번호 추가 API ", async (done) => {
  const response = await request(app)
    .post("/api/v1/jumin")
    .send({
      jumin: "881124-1234564"
    });

  expect(response.status).toBe(201);
  expect(response.body.data).toHaveProperty("juminNumber");
  expect(response.body.data).toHaveProperty("isRightJumin");

  done();
});

test("주민번호 리스트 API", async (done) => {
  const response = await request(app).get("/api/v1/jumin");

  expect(response.status).toBe(200);
  expect(response.body.data).toHaveProperty("juminList");
  done();
});


test("주민번호 삭제 API", async (done) => {
  const jumin = await Jumin.findOne({}, {
    id: db.sequelize.random(),
  });

  const response = await request(app).delete(`/api/v1/jumin/${jumin.id}`);
  expect(response.status).toBe(200);
  done();
});