/**
 * 주민 번호 검증 함수
 */
exports.verifyJumin = (jumin) => {
  /**
   * 검증하는 수 (12자리)
   * 주민 번호에서 - 제거  replace
   * 마지막 1자리 제거  slice
   */
  const verifingNumber = jumin.replace("-", "").slice(0, -1);
  /**
   * 검증되어지는 수 (마지막 1자리)
   * 문자를 숫자로 변환 Number
   */
  const verifiedNumber = Number(jumin[jumin.length - 1]);

  /**
   * 검증하는 수의 특정 연산의 합
   */
  let sumOfVerifingNumber = 0;
  let count = 2;
  for (let i = 0; i < verifingNumber.length; i++) {
    sumOfVerifingNumber += Number(verifingNumber[i]) * count; //검증하는 수의 특정 연산
    if (count >= 9) {
      count = 1;
    }
    count += 1;
  }

  /**
   * 주민등록 번호 검증
   */
  let isRightJumin = false;
  if (sumOfVerifingNumber % 11 === 11 - verifiedNumber) {
    isRightJumin = true;
  }

  return isRightJumin;
};