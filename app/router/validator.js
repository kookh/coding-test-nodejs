const {
  check
} = require('express-validator');

exports.jumin = [check("jumin")
  .isString()
  .withMessage("문자열이어야 합니다.")
  .isLength({
    max: 14,
    min: 14,
  })
  .withMessage("14자리의 주민번호이어야 합니다."),
];