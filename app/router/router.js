const validator = require("./validator");
const juminController = require("../controller/jumin.controller.js");
const realtimeController = require("../controller/realtime.controller.js");

module.exports = function (app) {
  //주민등록 번호 저장
  app.post(
    "/api/v1/jumin",
    validator.jumin,
    juminController.create
  );
  //주민등록 번호 리스트
  app.get("/api/v1/jumin", juminController.get);
  //주민등록 번호 삭제
  app.delete("/api/v1/jumin/:id", juminController.delete);
  //네이버 실시간 검색 목록 
  app.get("/api/v1/realtime", realtimeController.get);
};