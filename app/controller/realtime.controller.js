const axios = require("axios"); //http 통신
const cheerio = require("cheerio"); //html 파싱

exports.get = async (req, res) => {
  try {
    const targetUrl = "https://datalab.naver.com/keyword/realtimeList.naver?age=all&where=main";
    const response = await axios.get(targetUrl); //특정 URL의 http 요청에 대한 응답 값(html) 가져오기
    const $ = cheerio.load(response.data); //파싱을 위한 html 데이터 로드
    const $bodyList = $("ul.ranking_list").children("li.ranking_item"); //실시간 검색어 관련 부분 모두 추출

    const date = $(".date_txt").text();
    const time = $(".time_txt").text();

    //순위와 제목을 key, value 형태로 저장
    let rankingList = []; //순위와 제목 리스트
    $bodyList.each(function (i) {
      rankingList[i] = {
        ranking: $(this).find(".item_num").text(),
        title: $(this).find(".item_title").text(),
      };
    });

    res.status(200).json({
      message: "네이버 실시간 검색어를 반환합니다.",
      data: {
        date: date,
        time: time,
        rankingList: rankingList,
      },
    });
  } catch (err) {
    console.error(err);
  }
};