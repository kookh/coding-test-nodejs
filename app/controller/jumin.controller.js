const db = require("../config/db.config.js");
const juminService = require("../service/jumin.service.js");
const Jumin = db.jumin;
const {
  validationResult
} = require("express-validator");


/**
 * 주민 등록 번호 저장 
 */
exports.create = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: errors.array(),
    });
  }
  const isRightJumin = await juminService.verifyJumin(req.body.jumin);

  //DB 저장 실패 예외코드 작성
  try {
    await Jumin.create({
      jumin_number: req.body.jumin,
      is_right_jumin: isRightJumin,
    });

    res.status(201).json({
      message: "주민등록 번호 저장 성공",
      data: {
        juminNumber: req.body.jumin,
        isRightJumin: isRightJumin,
      },
    });
  } catch (err) {
    console.error(err);
  }
};

/**
 * 주민등록번호 리스트
 */
exports.get = async (req, res) => {
  //DB 조회 실패 예외코드 작성
  try {
    const juminList = await Jumin.findAll({
      order: [
        ["created_at", "DESC"]
      ],
      attributes: ["id", "jumin_number", "is_right_jumin", "created_at"],
    });

    res.status(200).json({
      message: "모든 주민등록번호 가져오기 성공",
      data: {
        juminList: juminList,
      },
    });
  } catch (err) {
    console.error(err);
  }
};

/**
 * 주민등록번호 삭제
 */
exports.delete = async (req, res) => {
  const jumin = await Jumin.findOne({
    where: {
      id: req.params.id
    }
  });
  if (!jumin) {
    res.status(404).json({
      message: "해당 id는 존재하지 않습니다.",
    });
    return;
  }

  //DB 삭제 실패 예외코드 작성
  try {
    await Jumin.destroy({
      where: {
        id: req.params.id,
      },
    });

    res.status(200).json({
      message: "주민등록번호 삭제 성공",
    });
  } catch (err) {
    console.error(err);
  }
};