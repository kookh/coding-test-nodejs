const env =
  process.env.NODE_ENV === "dev" ? {
    database: "codingtest",
    username: "root",
    password: "1234",
    host: "192.168.99.100",
    port: 3306,
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  } : {
    //production
    database: "codingtest",
    username: "root",
    password: "1234",
    host: "192.168.99.100",
    port: 3306,
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  };

module.exports = env;