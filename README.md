1. 네이버 실시간 검색어 가져오기
2. 유효한 주민등록 번호인지 판단

- 주민등록번호 저장 / 리스트 / 삭제
  ![테스트](./image/test.png)


### 1. 기술스택

### node.js, express.js, sequelize.js, mysql

### 2. MVC 패턴

##### 라우터 : app/router/router.js

##### 입력값 검증 : app/router/validator.js

##### 컨트롤러 : app/controller

##### 모델 : app/model

### 3. 테스트코드 (API 테스트, 단위 테스트)

##### app/test

### 4. 서비스코드

##### api/service

##### 주민번호 검증 함수

### 5. 설정 파일

##### api/config (mysql, sequelize)

##### docker-compose.yml (mysql 로컬 설치)

##### bitbucket-pipelines.yml (AWS IAM과 Bitbucket 파이프라인을 활용한 CI/CD)

### 6. 배포 (AWS EB, AWS RDS)
